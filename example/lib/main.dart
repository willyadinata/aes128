import 'dart:async';

import 'package:aes128/aes128.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String aes128Progress;

    //string format
    var data = "Hello World!";

    //create 16 byte random key
    var key = await (Aes128.generateDesKey(128));

    print(key);
    //encrypt
    var encryptText = await (Aes128.encryptString(data, key!));

    print(encryptText);
    //decrypt
    var decryptText = await (Aes128.decryptString(encryptText!, key));

    print(decryptText);

    aes128Progress = "data:" +
        data +
        "\n" +
        "create key:" +
        key +
        "\n" +
        "encryptText :" +
        encryptText +
        "\n" +
        "decryptText :" +
        decryptText! +
        "\n";

    print(aes128Progress);
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = aes128Progress;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('aes128'),
        ),
        body: Center(
          child: Text('aes128Progress:\n $_platformVersion\n'),
        ),
      ),
    );
  }
}
